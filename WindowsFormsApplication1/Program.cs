﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if DEBUG
            kernel32DLL.AllocConsole();
//            Debug.Listeners.Add(new ConsoleTraceListener());

            Debug.Listeners.Add(
                    new TextWriterTraceListener(new FileStream("DebugLog.txt", FileMode.Create, FileAccess.ReadWrite,
                            FileShare.ReadWrite, 1)));

            Debug.AutoFlush = true;
#endif

            Debug.WriteLine("Debuging starts ...");
            Debug.WriteLine(DateTime.Now.ToString());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
