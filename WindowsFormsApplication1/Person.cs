﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public class Person : IPerson
    {
        public Person(string name)
        {
            Id = 0;
            Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public void SayHi(string day)
        {
            MessageBox.Show($"Hey Dude! {Name} says Hi; \n\n Is this {day} holiday?");
        }
    }
}