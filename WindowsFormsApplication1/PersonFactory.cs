﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class PersonFactory
    {
        public static IPerson Create()
        {
            var repository = new Person("Alex");
            var dynamicProxy = new DynamicProxy<IPerson>(repository);
            return dynamicProxy.GetTransparentProxy() as IPerson;
        }
    }
}
