﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public interface IPerson
    {
        int Id { get; set; }

        string Name { get; set; }

        void SayHi(string day);
    }

}