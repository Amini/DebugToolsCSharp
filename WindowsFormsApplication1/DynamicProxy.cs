﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;

namespace WindowsFormsApplication1
{
    public class DynamicProxy<T> : RealProxy
    {
        private readonly T _decorated;

        public DynamicProxy(T decorated)
            : base(typeof (T))
        {
            _decorated = decorated;
        }

        private void Log(string msg, object arg = null, ConsoleColor foregroundColor = ConsoleColor.Green)
        {
            Console.ForegroundColor = foregroundColor;
            Console.WriteLine(msg, arg);
            Console.ResetColor();
            Debug.WriteLine(msg, arg);
        }

        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = msg as IMethodCallMessage;
            var methodInfo = methodCall.MethodBase as MethodInfo;
            Log("In Dynamic Proxy - Before executing '{0}'",
                methodCall.MethodName, ConsoleColor.Green);
            Log("The method has {0} arguments", methodCall.ArgCount, ConsoleColor.DarkGray);

            if (methodInfo != null)
                Log("The method return Type is {0}", methodInfo.ReturnType, ConsoleColor.DarkGray);
            var s = "";
            for (int index = 0; index < methodCall.Args.Length; index++)
            {
                var arg = methodCall.Args[index];
                var name = methodCall.GetArgName(index);
                s += $"{name} = {arg}\n";
            }
            Log("The method Arg values are \n{0}", s, ConsoleColor.DarkGray);
            try
            {
                var result = methodInfo.Invoke(_decorated, methodCall.InArgs);
                Log("In Dynamic Proxy - After executing '{0}' ",
                    methodCall.MethodName, ConsoleColor.Green);

                return new ReturnMessage(result, null, 0,
                    methodCall.LogicalCallContext, methodCall);
            }
            catch (Exception e)
            {
                Log(string.Format(
                    "In Dynamic Proxy- Exception {0} executing '{1}'", e),
                    methodCall.MethodName, ConsoleColor.Green);
                return new ReturnMessage(e, methodCall);
            }
        }
    }
}